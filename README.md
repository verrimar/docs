# Project 5 : Embedded equipment web service communicating on CAN bus by Liam ANDRIEUX, Lucas DREZET and Roman REGOUIN

## Document's links:
- [Follow-up sheet](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/docs/-/blob/master/EmbeddedEquipmentWebServiceCommunicatingOnCANBus_info4_2020_2021.md "Follow-up sheet")
- [Report](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/docs/-/blob/master/Report.pdf "Final report")
- [Pre-viva presentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/docs/-/blob/master/Pre%20viva%20presentation.pdf "Pre-viva presentation")
- [Final viva presentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/docs/-/blob/master/Final%20viva%20presentation.pdf "Final viva presentation")

## Our repositories links:
- [Documentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/Documentation "Documentation")
- [Raspberry](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/raspberry "Raspberry"): Raspberry code (**OUTDATED**)
- [STM32F4 simulateur CAN bus](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F4_CAN "STM32F4 CAN"): CAN bus simulator
- [STM32F7 CycloneTCP](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer "STM32F7 CycloneTCP"): websocket web server (**FINAL VERSION**) and ajax web server (**OUTDATED**)
- [STM32F7 Berkeley Socket](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_BerkeleySocketsServer "STM32F7 BerkeleySocket"): first embedded web server (**OUTDATED**)
- [STM32F7 Minnow](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MinnowServer "STM32F7 Minnow"): websocket web server (**ABORTED**)
- [STM32F7 Mongoose](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_MongooseServer "STM32F7 Mongoose"): websocket web server (**ABORTED**)
